const HTTP = require("http");

HTTP.createServer((request, response) => {

    if(request.url === "/login"){
        response.writeHead(200, {"Content-type": "text/plain"});
        response.write("Welcome to the login page.");
        response.end();
    }
    else{
        response.writeHead(404, {"Content-type": "text/plain"});
        response.write("I'm sorry the page you are looking for cannot be found.");
        response.end();
    }

}).listen(3001)

//console.log("Server is successfully running.");
