//The questions are as follows: 
//What directive is used by Node.js in loading the modules it needs? 
=require
//What Node.js module contains a method for server creation? 
= HTTP 
//What is the method of the http object responsible for creating a server using Node.js? 
=HTTP.createServer(request, response)
//What method of the response object allows us to set status codes and content types?
=response.writeHead()
//Where will console.log() output its contents when run in Node.js?
=terminal
//What property of the request object contains the address' endpoint?
=request.url 
